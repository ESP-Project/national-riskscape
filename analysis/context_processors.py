from django.conf import settings
from analysis.views import get_quarter
from analysis.utils import get_time_range
import logging 

logger = logging.getLogger('national_riskscape')

def geo_config(request):
    return {'GEO_PLACES': settings.GEO_PLACES}


def organization(request):
    return {'ORGANIZATION': settings.ORGANIZATION}


def org_system(request):
    return {'ORG_SYSTEM': settings.ORG_SYSTEM}


def coverage_area(request):
    return {'COVERAGE_AREA': settings.COVERAGE_AREA}


def about_text(request):
    return {'ABOUT_TEXT': settings.ABOUT_TEXT}

def google_id(request):
    return {'GOOGLE_TRACKING_ID': settings.GOOGLE_TRACKING_ID}

def google_form(request):
    return {'GOOGLE_FORM': settings.GOOGLE_FORM}

def feedback_text(request):
    return {'FEEDBACK_TEXT': settings.FEEDBACK_TEXT}


def date_range(request):
    try:
        drm, dry, start, end = get_time_range('state')

        months = [{'display': m.format("MMM `YY"), 'month': m.month, 'year': m.year}
                  for
                  m in drm]
        years = [{'display': y.format('YYYY'), 'month': 1, 'year': y.year} for y in dry]

        context = {'month_range': months, 'quarter_range': None, 'year_range': years}
        context['default_start'] = {'quarter': get_quarter(start.month), 'month': start.month, 'year': start.year}
        context['default_end'] = {'quarter': get_quarter(end.month), 'month': end.month, 'year': end.year}

        return context
    except:
        logger.debug("date range error in analysis context")
        return {}

