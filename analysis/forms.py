from django import forms
from modeled_data.models import (ModeledData, NationalLevel, StateLevel, 
                                CountyLevel, ZipLevel)
from modeled_data.models import CONDITIONS, EST_TYPE, DEMOGRAPHICS
from modeled_data.models import RURAL_URBAN, STATE_RURAL_URBAN
from modeled_data.models import SEX, AGE_GROUP, RACE, PRIMARY_PAYER, OVERALL
from django.conf import settings

INIT_CONDITION = {'condition':'HTN'}
INIT_FILTER = {'est_type':'modeled',
               'demographic':'overall',
               'sex':'female',
               'age_group':'20-24',
               'race':'hispanic',
               'rural_urban':'urban',
               'primary_payer':'commercial',
               'county_zip_tract':'county'}

INIT_GEO = {"geo_area":settings.GEO_AREA_DEFAULT}
INIT_GEO = {"geo_area":settings.GEO_AREA_DEFAULT}
if settings.CENSUS_TRACT:
    COUNTY_ZIP_TRACT = (('county','county'),('zip','zip code'), ('tract','census tract'))
else:
    COUNTY_ZIP_TRACT = (('county','county'),('zip','zip code'))

class GeoForm(forms.Form):
        geo_area = forms.ChoiceField(
            widget=forms.Select(),
            choices=settings.GEO_PLACE_CHOICES,
            initial=settings.GEO_AREA_DEFAULT,
        )

def process_queryform(request, outcome=None):
    if request.method == 'POST':
        outcome_form = OutcomeQueryForm(request.POST, prefix="outcome-form", initial=INIT_CONDITION)
        filter_form = FilterQueryForm(request.POST, prefix="filter-form", initial=INIT_FILTER)
        geo_form = GeoForm(request.POST, initial=INIT_GEO)
    else:
        outcome_form = OutcomeQueryForm(INIT_CONDITION,prefix="outcome-form")
        filter_form = FilterQueryForm(INIT_FILTER,prefix="filter-form")
        geo_form = GeoForm(INIT_GEO)

    return outcome_form, filter_form, geo_form

class OutcomeQueryForm(forms.Form):
    condition = forms.ChoiceField(
            widget=forms.Select(),
            choices=CONDITIONS,
            )

class FilterQueryForm(forms.Form):
    est_type = forms.ChoiceField(
            widget=forms.Select(),
            choices=EST_TYPE,
            )
    demographic = forms.ChoiceField(
            widget=forms.Select(),
            choices=DEMOGRAPHICS,
            )
    sex = forms.ChoiceField(
            widget=forms.Select(),
            choices=SEX,
            )
    age_group = forms.ChoiceField(
            widget=forms.Select(),
            choices=settings.AGE_GROUP,
            )
    race = forms.ChoiceField(
            widget=forms.Select(),
            choices=settings.RACE,
            )
    primary_payer = forms.ChoiceField(
            widget=forms.Select(),
            choices=settings.PRIMARY_PAYERS,
            )
    rural_urban = forms.ChoiceField(
            widget=forms.Select(),
            choices=settings.RURAL_URBAN,
            )
    county_zip_tract = forms.ChoiceField(
            widget=forms.RadioSelect(),
            choices=COUNTY_ZIP_TRACT,
            )
    min_prevalence = forms.IntegerField(
            required = False,
            max_value = 100,
            min_value = 0,
            widget=forms.NumberInput(),
            )
    max_prevalence = forms.IntegerField(
            required = False,
            max_value = 100,
            min_value = 0,
            widget=forms.NumberInput(),
            )
