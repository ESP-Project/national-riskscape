from math import ceil, floor
from django_rq import job
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

from analysis.forms import OutcomeQueryForm, FilterQueryForm
from analysis.models import MapResultCache, CurrentRateResultCache
from analysis.models import TimeseriesResultCache, County
from analysis.utils import get_time_range, year_month_int, year_month_str
from modeled_data.models import NationalLevel, StateLevel, CountyLevel, ZipLevel, CensusLevel
from modeled_data.models import STATES
from modeled_data.models import RURAL_URBAN, STATE_RURAL_URBAN
from modeled_data.models import SEX, AGE_GROUP, RACE, PRIMARY_PAYER, OVERALL
import logging 

logger = logging.getLogger('national_riskscape')

if settings.STAGING:
    default_rq = 'default-stg'
else:
    default_rq = 'default'

def _get_filler_point(calendar):
    return {"year": calendar.year, 'month': calendar.month, 'prevalence': -1}

def set_decimals(ratio):
    if ratio > 1:
        return "{:01.1f}".format(ratio)
    else:
        return "{:01.2f}".format(ratio)

def save_strat_result(crc, result):
    strat_result = []
    for strat in result:
        if strat.demographic and strat.demographic == crc.stratify:
            demographic_result = {strat.demographic:strat.demographic_value.lower(),
                                  "prevalence":strat.prevalence*100 if strat.prevalence else None,
                                  "se":strat.se,
                                  "npats":strat.npats}
            strat_result.append(demographic_result)
    if (len(strat_result) > 0):
        crc.result = strat_result
    else:
        crc.result = False

def save_strat_result2(crc, result):
    strat_result = []
    for strat in result:
        if crc.stratify == 'overall':
            demographic_result = {'overall':'overall',
                                  "prevalence":strat.prevalence*100 if strat.prevalence else None,
                                  "se":strat.se,
                                  "npats":strat.npats}
            strat_result.append(demographic_result)
        else:
            demographic_result = {'rural_urban':strat.rural_urban.lower(),
                                  "prevalence":strat.prevalence*100 if strat.prevalence else None,
                                  "se":strat.se,
                                  "npats":strat.npats}
            strat_result.append(demographic_result)
    if (len(strat_result) > 0):
        crc.result = strat_result
    else:
        crc.result = False

@job(default_rq)
def generate_map_cache(cache_pk, geo_area, month=None, year=None):
    mrc = MapResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(mrc.outcome)
    filter_form = FilterQueryForm(mrc.filter)
    condition = None
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not available for map cache result.")

    map_data = []
    md_range = {"min": float(100), "max": float(0)}
    national_prevalence = None
    national_npats = None
    national_se = None
    national_lowerci = None
    national_upperci = None
    national_place_name = 'United States'
    us_states = dict((x,y) for x,y in STATES)
    if filter_form.is_valid():
        est_type = filter_form.cleaned_data['est_type']
        demographic = filter_form.cleaned_data['demographic']
        county_zip_tract = filter_form.cleaned_data['county_zip_tract']
        min_prevalence = filter_form.cleaned_data['min_prevalence']
        max_prevalence = filter_form.cleaned_data['max_prevalence']
    else:
        est_type = 'modeled'
        demographic = 'overall'
        county_zip_tract = 'county'
        min_prevalence = None
        max_prevalence = None
    if condition:
        min_prev_valid = min_prevalence is not None and min_prevalence >= 0 and min_prevalence <= 100
        max_prev_valid = max_prevalence is not None and max_prevalence >= 0 and max_prevalence <= 100
        if demographic and demographic == 'overall':
            demographic_value = 'overall'
        elif demographic and filter_form.is_valid():
            demographic_value = filter_form.cleaned_data[demographic]
        else:
            demographic_value = None
        if demographic_value:
            year_month = year_month_str(year, month, '-')
            geo_places = settings.GEO_PLACES
            geo_place = geo_places[geo_area]
            place_geo_area = geo_place['GEO_AREA']
            place_geo_code = geo_place['GEO_CODE']
            if place_geo_area == 'statecode':
                # national map
                national_model = NationalLevel
                state_model = StateLevel
            elif county_zip_tract == 'zip':
                # state zip code map
                place_geo_area = geo_place['GEO_AREA2']
                national_model = StateLevel
                state_model = ZipLevel
            elif county_zip_tract == 'tract':
                # state census tract map
                place_geo_area = geo_place['GEO_AREA3']
                national_model = StateLevel
                state_model = CensusLevel
            elif county_zip_tract == 'county':
                # state county map
                national_model = StateLevel
                state_model = CountyLevel
            try:
                if place_geo_area == 'statecode':
                    national_data = national_model.objects.get(year_month=year_month,
                                                 condition=condition,
                                                 demographic=demographic,
                                                 demographic_value__iexact=demographic_value,
                                                 est_type=est_type)
                else:
                    national_place_name = us_states[place_geo_code] if place_geo_code else place_geo_code
                    national_data = national_model.objects.get(year_month=year_month,
                                                 condition=condition,
                                                 demographic=demographic,
                                                 demographic_value__iexact=demographic_value,
                                                 est_type=est_type,
                                                 state=place_geo_code)
                national_prevalence = national_data.prevalence
                national_npats = national_data.npats
                national_se = national_data.se
                nci = 1.96*float(national_se) if national_se else None
                national_lowerci = float(national_prevalence) - nci if national_prevalence and nci else None
                national_upperci = float(national_prevalence) + nci if national_prevalence and nci else None
                national_se = round(national_se,3) if national_se else None
                national_lowerci = round(national_lowerci,3) if national_lowerci else national_lowerci    
                national_upperci = round(national_upperci,3) if national_upperci else national_upperci
            except ObjectDoesNotExist:
                logger.debug("National data is not available for map cache")
            
            if place_geo_area == 'statecode':
                state_data = state_model.objects.filter(year_month=year_month,
                                            condition=condition,
                                            demographic=demographic,
                                            demographic_value__iexact=demographic_value,
                                            est_type=est_type)
            else:
                if demographic == 'overall':
                    state_data = state_model.objects.filter(year_month=year_month,
                                            condition=condition,
                                            est_type=est_type,
                                            state=place_geo_code)
                elif demographic == 'rural_urban':
                    state_data = state_model.objects.filter(year_month=year_month,
                                            condition=condition,
                                            rural_urban=demographic_value,
                                            est_type=est_type,
                                            state=place_geo_code)

            for s in state_data:
                ci = 1.96* float(s.se) if s.se else None
                upper_ci = (float(s.prevalence) + ci) if ci and s.prevalence else None
                lower_ci = (float(s.prevalence) - ci) if ci and s.prevalence else None
                upper_ci = round(upper_ci,3) if upper_ci else upper_ci
                lower_ci = round(lower_ci,3) if lower_ci else lower_ci
                state_se = round(s.se,3) if s.se else None
                place_code = None
                place_name = None
                place_lat = None
                place_lon = None
                if place_geo_area == 'statecode':
                    place_code = s.state
                    place_name = us_states[place_code] if place_code else place_code
                    place_name = 'State: ' + place_name
                elif place_geo_area == 'postcode':
                    place_code = s.zip
                    place_name = 'Zip Code: ' + place_code
                elif place_geo_area == 'countycode':
                    place_code = s.county_fips
                    place_code = place_code[2:]
                    try:
                        county = County.objects.get(fips=s.county_fips)
                        place_name = county.name
                        place_lat = county.lat
                        place_lon = county.lon
                    except ObjectDoesNotExist:
                        place_name = 'County code: ' + place_code
                
                # set min/max prevalence to user selected values if available
                # and limit prevalence to min/max values
                if min_prev_valid and max_prev_valid and s.prevalence:
                    md_range['min'] = min_prevalence
                    md_range['max'] = max_prevalence
                    if (s.prevalence*100 < min_prevalence):
                        range_prevalence = min_prevalence
                        MapResultCache.objects.all().delete()
                    elif (s.prevalence*100 > max_prevalence):
                        range_prevalence = max_prevalence
                        MapResultCache.objects.all().delete()
                    else:
                        range_prevalence = s.prevalence*100
                elif s.prevalence:
                    range_prevalence = s.prevalence*100
                    md_range['min'] = min(float(range_prevalence), md_range['min'])
                    md_range['max'] = max(float(range_prevalence), md_range['max'])
                else:
                    range_prevalence = None

                md_npats = int(s.npats) if s.npats else None
                md_npats = f"{md_npats:,d}" if md_npats else None
                map_data.append({'zip': place_code,
                                 'place_name': place_name,
                                 'place_lat': place_lat,
                                 'place_lon': place_lon,
                                 'prevalence': s.prevalence*100 if s.prevalence else None,
                                 'range_prevalence': range_prevalence,
                                 'population': md_npats,
                                 'se': state_se,
                                 'lowerci': lower_ci,
                                 'upperci': upper_ci,
                                 })


            if md_range['max'] == md_range['min'] == float(100):
                md_range['min'] = 0

            if md_range['max'] > md_range['min']:
                for md in map_data:
                    if md['prevalence'] is not None:
                        md['normalized'] = (md['range_prevalence'] - md_range['min']) / (md_range['max'] - md_range['min'])
                        md['prevalence'] = set_decimals(md['prevalence'])
            else:
                md_range['min'] = md_range['max']
            md_range['max'] = int(ceil(md_range['max']))
            md_range['min'] = int(floor(md_range['min']))
            
        else:
            logger.debug("No demographic value for map cache")
    else:
        logger.debug("Demographic parameters are not availble for map cache result.")
    national_prevalence = round(national_prevalence*100,2) if national_prevalence else None
    national_npats = int(national_npats) if national_npats else None
    national_npats = f"{national_npats:,d}" if national_npats else None
    nationwide = {"prevalence": national_prevalence, 
                  "npats": national_npats,
                  "se": national_se,
                  "lowerci": national_lowerci,
                  "upperci": national_upperci,
                  "place_name": national_place_name,
                  }
    result = {"map_data": map_data,
              "map_range": md_range,
              "nationwide": nationwide}
    mrc.result = result
    mrc.save()

@job(default_rq)
def generate_current_rate_cache(cache_pk, geo_area, month, year):

    crc = CurrentRateResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(crc.outcome)
    filter_form = FilterQueryForm(crc.filter)
    condition = None
    stratify = crc.stratify 
    location = crc.location
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not availble for current rate cache result.")

    est_type = filter_form.cleaned_data['est_type'] if filter_form.is_valid() else 'modeled'
    if stratify:
        year_month = year_month_str(year, month, '-')
        try:
            if location == 'US':
                result = NationalLevel.objects.filter(year_month=year_month,
                                                 condition=condition,
                                                 demographic=stratify,
                                                 est_type=est_type)
                save_strat_result(crc, result)

            else:
                place_geo_code = settings.GEO_PLACES[geo_area]['GEO_CODE']
                is_state = [location for i in STATES if location in i]
                if is_state:
                    result = StateLevel.objects.filter(year_month=year_month,
                                                 condition=condition,
                                                 demographic=stratify,
                                                 est_type=est_type,
                                                 state=location)
                    save_strat_result(crc, result)
                else:
                    county_data = CountyLevel.objects.filter(year_month=year_month,
                                            condition=condition,
                                            est_type=est_type,
                                            county_fips=location,
                                            state=place_geo_code)
                    if county_data:
                        save_strat_result2(crc, county_data)
                    else:
                        zip_data = ZipLevel.objects.filter(year_month=year_month,
                                            condition=condition,
                                            est_type=est_type,
                                            zip=location,
                                            state=place_geo_code)
                        if zip_data:
                            save_strat_result2(crc, zip_data)
                        else:
                            ctract_data = CensusLevel.objects.filter(year_month=year_month,
                                            condition=condition,
                                            est_type=est_type,
                                            census_tract=location)
                            if ctract_data:
                                save_strat_result2(crc, ctract_data)

        except ObjectDoesNotExist:
            logger.debug("Data is not available for current rate cache")

        crc.save()

@job(default_rq)
def generate_timeseries_cache(cache_pk, geo_area):
    trc = TimeseriesResultCache.objects.get(pk=cache_pk)
    outcome_form = OutcomeQueryForm(trc.outcome)
    filter_form = FilterQueryForm(trc.filter)
    condition = None
    stratify = None 
    location = trc.location
    if outcome_form.is_valid():
        condition = outcome_form.cleaned_data['condition']
    else:
        logger.debug("Condition is not availble for timeseries cache result.")
    if filter_form.is_valid() and condition:
        stratify = filter_form.cleaned_data['demographic']
        est_type = filter_form.cleaned_data['est_type']
    else:
        logger.debug("Filter form is not valid for timeseries cache result.")

    model_data = None
    data_level=None
    if stratify =='overall':
        if location == 'US':
            model_data = NationalLevel.objects.filter(
                                                 condition=condition,
                                                 est_type=est_type)
            data_level = 'national'
        else:
            place_geo_code = settings.GEO_PLACES[geo_area]['GEO_CODE']
            is_state = [location for i in STATES if location in i]
            if is_state:
                model_data = StateLevel.objects.filter(
                                                 condition=condition,
                                                 est_type=est_type,
                                                 state=location)
                data_level = 'state'
            else:
                county_data = CountyLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            county_fips=location,
                                            state=place_geo_code)
                if county_data:
                    model_data = county_data
                    data_level = 'county'
                else:
                    zip_data = ZipLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            zip=location,
                                            state=place_geo_code)
                    if zip_data:
                        model_data = zip_data
                        data_level = 'zip'
                    else:
                        tract_data = CensusLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            census_tract=location)
                        if tract_data:
                            model_data = tract_data
                            data_level = 'tract'
        if model_data:
            month_range, year_range, start, end = get_time_range(data_level)
            irange = iter(month_range)
            result = {'All': []}
            while True:
                try:
                    calendar = next(irange)
                    result['All'].append(_get_filler_point(calendar))
                except StopIteration:
                    break
            for md in model_data.order_by('year_month'):
                for filler in result['All']:
                    year,month = year_month_int(md.year_month)
                    if filler['year'] == year and filler['month'] == month and md.prevalence:
                        filler['prevalence'] = md.prevalence*100
                        break
        else:
            result = []

    else:
        strat_by=None
        if location == 'US':
            model_data = NationalLevel.objects.filter(
                                                 condition=condition,
                                                 demographic=stratify,
                                                 est_type=est_type)
            strat_by = 'demographic_value'
            data_level = 'national'
        else:
            place_geo_code = settings.GEO_PLACES[geo_area]['GEO_CODE']
            is_state = [location for i in STATES if location in i]
            if is_state:
                model_data = StateLevel.objects.filter(
                                                 condition=condition,
                                                 demographic=stratify,
                                                 est_type=est_type,
                                                 state=location)
                strat_by = 'demographic_value'
                data_level = 'state'
            else:
                strat_by = 'rural_urban'
                county_data = CountyLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            county_fips=location,
                                            state=place_geo_code)
                if county_data:
                    model_data = county_data
                    data_level = 'county'
                else:
                    zip_data = ZipLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            zip=location,
                                            state=place_geo_code)
                    if zip_data:
                        model_data = zip_data
                        data_level = 'zip'
                    else:
                        tract_data = CensusLevel.objects.filter(
                                            condition=condition,
                                            est_type=est_type,
                                            census_tract=location)
                        if tract_data:
                            model_data = zip_data
                            data_level = 'tract'
        
        result = {}
        if model_data:
            month_range, year_range, start, end = get_time_range(data_level)
            strat_values = {}
            if stratify == 'rural_urban':
                strat_values = dict((x,y) for x,y in settings.RURAL_URBAN)
            elif stratify == 'sex':
                strat_values = dict((x,y) for x,y in SEX)
            elif stratify == 'age_group':
                strat_values = dict((x,y) for x,y in settings.AGE_GROUP)
            elif stratify == 'race':
                strat_values = dict((x,y) for x,y in RACE)
            elif stratify == 'primary_payer':
                strat_values = dict((x,y) for x,y in settings.PRIMARY_PAYERS)

            irange = iter(month_range)
            for strat,v in strat_values.items():
                result[strat.lower().replace("/","_")] = []
            while True:
                try:
                    calendar = next(irange)
                    for strat,v in strat_values.items():
                        result[strat.lower().replace("/","_")].append(_get_filler_point(calendar))
                except StopIteration:
                    break
            for md in model_data.order_by('year_month'):
                if strat_by == 'demographic_value':
                    strat = md.demographic_value
                elif strat_by == 'rural_urban':
                    strat = md.rural_urban
                else:
                    strat = None
                if strat:
                    strat = strat.lower().replace("/","_")
                    for filler in result[strat]:
                        year,month = year_month_int(md.year_month)
                        if filler['year'] == year and filler['month'] == month and md.prevalence:
                            filler['prevalence'] = md.prevalence*100
                            break

    try:
        trc.result = [dict(x) for x in result]
    except ValueError:
        trc.result = result

    trc.save()
