from analysis import forms
from django.utils.deprecation import MiddlewareMixin

class OutcomeFormMiddleware(MiddlewareMixin):
    def process_template_response(self, request, response):
        if request.path not in ['/about/','/api/token/']:
            try:
                ocf = response.context_data["outcome_form"]
                ff = response.context_data["filter_form"]
                response.context_data["outcome_form"] = forms.reinitialize(ocf)
                response.context_data["filter_form"] = forms.reinitialize(ff)
                response.context_data["outcome_selected"] = ocf.pretty_print_query()
                response.context_data["filter_selected"] = ff.pretty_print_query()
            except KeyError:
                pass

        return response
