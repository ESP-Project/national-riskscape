import json
from datetime import date
import hashlib
import arrow
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import F
from django.conf import settings
import logging 

logger = logging.getLogger('national_riskscape')

ENCOUNTER_FILTER = {'encounters_one_year': 'Historic', 'encounters_total': 1}
ALL_CONDITIONS = {
    "Type 2 Diabetes":
        {"outcome": {'dm2': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Smoking":
        {"outcome": {'smoker': [4, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Hypertension":
        {"outcome": {'hypertension': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Pediatric Asthma":
        {"outcome": {'asthma': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [1, 2, 3, 4]})
         },
    "Obesity":
        {"name": "Obesity (BMI &gt;30)",
         "outcome": {'bmi': [3, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Overweight":
        {"name": "Overweight (BMI 25-30)",
         "outcome": {'bmi': [2, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Influenza like illness Monthly":
        {"name": "Influenza-like illness (Monthly)",
         "outcome": {'ili_current': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Influenza like illness Cumulative":
        {"name": "Influenza-like illness (Cumulative)",
         "outcome": {'ili_cum': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Curr ILI vac":
        {"name": "Influenza vaccination, current season",
         "outcome": {'curr_flu_vac': [1, ]},
         "filter": dict(ENCOUNTER_FILTER)
         },
    "Lyme disease Monthly":
        {"name": "Lyme disease (Monthly)",
         "outcome": {'lyme': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Lyme disease Cumulative":
        {"name": "Lyme disease (Cumulative)",
         "outcome": {'lyme_cum': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Pertussis syndrome Monthly":
        {"name": "Pertussis syndrome (Monthly)",
         "outcome": {'pertussis': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Pertussis syndrome Cumulative":
        {"name": "Pertussis syndrome (Cumulative)",
         "outcome": {'pertussis_last': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Syphilis Test Pregnant":
        {"name": "Syphilis screening in pregnant women",
         "outcome": {'syph_test': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"sex": [2, ], 'pregnant': [1, ]})
         },
    "Hepc Boomer":
        {"name": "Hepatitis C screening in patients born 1945-1965",
         "outcome": {'hepc_test': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"birth_cohort": [1, ]})
         },
    "Young Female Chlamy":
        {"name": "Chlamydia screening in women aged 15-24",
         "outcome": {'chlamydia': [1, 2, 3, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"sex": [2, ], "age_group": [4, 5]})
         },
    "Depression":
        {"name": "Depression (Rx)",
         "outcome": {'depression': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Opioid Rx":
        {"name": "Opioid Prescription",
         "outcome": {'opioid_rx': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
}

DASHBOARD_CONDITIONS = {}
for cond in settings.RISKSCAPE_CONDITIONS:
    DASHBOARD_CONDITIONS[cond] = ALL_CONDITIONS[cond]

class ResultCacheManager(models.Manager):
    def cache_results(self, **kwargs):
        try:
            rc = self.get(**kwargs)
        except ObjectDoesNotExist:
            rc = self.create(**kwargs)
        if kwargs['result'] is not None:
            rc.result = kwargs['result']
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def _get_cache(self, **kwargs):
        rc = self.get(**kwargs)
        if rc.result is not None:
            rc.viewed = F('viewed') + 1
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def get_cached_results(self, **kwargs):
        try:
            trc = self._get_cache(**kwargs)
            return trc
        except self.model.DoesNotExist:
            return None
        except self.model.MultipleObjectsReturned:
            return None

    def get(self, **kwargs):
        try:
            kwargs['geo_hash'] = hashlib.md5(json.dumps(kwargs['geo_form'].cleaned_data).encode('utf-8')).hexdigest()
            #kwargs['outcome_hash'] = kwargs['outcome_form'].query_hash()
            kwargs['outcome_hash'] = hashlib.md5(json.dumps(kwargs['outcome_form'].cleaned_data).encode('utf-8')).hexdigest()
            #kwargs['filter_hash'] = kwargs['filter_form'].query_hash()
            kwargs['filter_hash'] = hashlib.md5(json.dumps(kwargs['filter_form'].cleaned_data).encode('utf-8')).hexdigest()
            del kwargs['outcome_form']
            del kwargs['filter_form']
            del kwargs['geo_form']
        except KeyError:
            logger.debug('result cache get KeyError')
            pass


        return super().get(**kwargs)


class ResultCache(models.Model):
    outcome_form = models.JSONField()
    filter_form = models.JSONField()
    geo_form = models.JSONField()
    outcome_hash = models.CharField(max_length=32)
    filter_hash = models.CharField(max_length=32)
    geo_hash = models.CharField(max_length=32)
    result = models.JSONField(null=True, blank=True)
    result2 = models.JSONField(null=True, blank=True)
    result3 = models.JSONField(null=True, blank=True)
    viewed = models.IntegerField(default=1)
    last_accessed = models.DateField(auto_created=True, null=True, blank=True)

    objects = ResultCacheManager()

    class Meta:
        abstract = True

    @property
    def outcome(self):
        return json.loads(self.outcome_form)

    @property
    def filter(self):
        return json.loads(self.filter_form)

    def save(self, *args, **kwargs):
        if self.pk is None:
            #self.outcome_hash = self.outcome_form.query_hash()
            self.outcome_hash = hashlib.md5(json.dumps(self.outcome_form.cleaned_data).encode('utf-8')).hexdigest()
            #self.filter_hash = self.filter_form.query_hash()
            self.filter_hash = hashlib.md5(json.dumps(self.filter_form.cleaned_data).encode('utf-8')).hexdigest()
            self.geo_hash = hashlib.md5(json.dumps(self.geo_form.cleaned_data).encode('utf-8')).hexdigest()
            self.outcome_form = json.dumps(self.outcome_form.cleaned_data, sort_keys=True)
            self.filter_form = json.dumps(self.filter_form.cleaned_data, sort_keys=True)
            self.geo_form = json.dumps(self.geo_form.cleaned_data, sort_keys=True)
        super(ResultCache, self).save(*args, **kwargs)

class MapResultCache(ResultCache):
    month = models.IntegerField(null=True)
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash','geo_hash', 'month', 'year')

class CurrentRateResultCache(ResultCache):
    location = models.CharField(max_length=32)
    stratify = models.CharField(max_length=32, null=True, blank=True)
    month = models.IntegerField(null=True)
    year = models.IntegerField(null=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 
                           'stratify', 'geo_hash', 'month', 'year')


class TimeseriesResultCache(ResultCache):
    location = models.CharField(max_length=32)
    time_chunk = models.CharField(max_length=10)
    stratify = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        unique_together = ('outcome_hash', 'filter_hash', 'location', 'stratify', 
                            'time_chunk', 'geo_hash')

class County(models.Model):
    name = models.CharField('name',
                            max_length=50,
                            null=False,
                            blank=False)
    fips = models.CharField('fips',
                            max_length=5,
                            null=False,
                            blank=False)
    lat = models.FloatField('latitude',
                                null=False,
                                )
    lon = models.FloatField('longititude',
                                null=False,
                                )


