import logging
logger = logging.getLogger(__name__)

class Stats(object):
    def __init__(self, data, *args, **kwargs):
        self.inflection = kwargs.get('inflection_point', 0)
        self.data = self._prep_data(data)
        self.data_frame = self._get_dataframe(self.data)

    def _prep_data(self, data):
        for datum in data:
            datum['time'] = int(datum['time'])
            datum['Comparison'] = float(datum.get('Comparison', 0))
            try:
                datum['Reference'] = float(datum['Reference'])
            except TypeError:
                datum['Reference'] = 0
            datum['lvchg'] = 0 if int(datum['time']) < self.inflection else 1
            datum['trchg'] = 0 if int(datum['time']) < self.inflection else (int(datum['time']) - self.inflection) + 1
            datum['dif'] = float(datum['Comparison']) - float(datum['Reference'])
        return data

    def _get_dataframe(self, data_dict):
        from pandas import DataFrame
        df = DataFrame(data_dict)
        df = df.dropna()
        return df

    def _get_endog_exog(self, formula):
        from patsy import dmatrices
        return dmatrices(formula, data=self.data_frame, return_type='dataframe')

    def _do_gls(self, formula):
        from scipy.linalg import toeplitz
        from statsmodels.api import OLS, GLS, add_constant
        try:
            endog, exog = self._get_endog_exog(formula)
        except Exception:
            logger.error('patsy dmatrices error')
            return None
        try:
            ols_resid = OLS(endog, exog).fit()._results.resid
            resid_fit = OLS(ols_resid[1:], add_constant(ols_resid[:-1])).fit()
            rho = resid_fit.params[1]
            order = toeplitz(range(len(ols_resid)))
            sigma = rho ** order
        except Exception:
            logger.error('statsmodel OLS error')
            return None
        try:
            gls_model = GLS(endog, exog, sigma=sigma)
            gls_results = gls_model.fit()
            return gls_results
        except Exception:
            logger.error('statsmodel GLS error')
            return None

    def _trend(self, gls, col_name):
        coefs = gls.params
        for datum in self.data:
            datum[col_name] = coefs[0] + \
                              (coefs[1] * datum['time'])

    def _inflect(self, gls, col_name):
        coefs = gls.params
        for datum in self.data:
            datum[col_name + '_coefs'] = coefs[0]
            datum[col_name + '_timecoefs'] = coefs[1]
            datum[col_name + '_lvchgcoefs'] = coefs[2]
            datum[col_name + '_trchgcoefs'] = coefs[3]
            datum[col_name] = coefs[0] + \
                              (coefs[1] * datum['time']) + \
                              (coefs[2] * datum['lvchg']) + \
                              (coefs[3] * datum['trchg'])

    def _ref_trend(self):
        formula = 'Reference ~ time'
        self.ref_trend = self._do_gls(formula)
        if self.ref_trend is not None:
            self._trend(self.ref_trend, 'lineReference')

    def _ref_inflect(self):
        formula = 'Reference ~ time+lvchg+trchg'
        self.ref_inflect = self._do_gls(formula)
        if self.ref_inflect is not None:
            self._inflect(self.ref_inflect, 'lineReference2')

    def _comparison_trend(self):
        formula = 'Comparison ~ time'
        self.comp_trend = self._do_gls(formula)
        if self.comp_trend is not None:
            self._trend(self.comp_trend, 'lineComparison')

    def _comparison_inflect(self):
        formula = 'Comparison ~ time+lvchg+trchg'
        self.comp_inflect = self._do_gls(formula)
        if self.comp_inflect is not None:
            self._inflect(self.comp_inflect, 'lineComparison2')

    def _dif_trend(self):
        formula = 'dif ~ time'
        self.dif_trend = self._do_gls(formula)
        if self.dif_trend is not None:
            self._trend(self.dif_trend, 'lineDif')

    def _dif_inflect(self):
        formula = 'dif ~ time+lvchg+trchg'
        self.dif_inflect = self._do_gls(formula)
        if self.dif_inflect is not None:
            self._inflect(self.dif_inflect, 'lineDif2')

    def _regression(self, in_name, col_name):
        for datum in self.data:
            if datum['time'] <= self.inflection:
                datum[col_name] = datum[in_name + '_coefs'] + \
                                  datum[in_name + '_timecoefs'] * datum['time']
            else:
                datum[col_name] = datum[in_name + '_coefs'] + \
                                  datum[in_name + '_timecoefs'] * datum['time'] + \
                                  datum[in_name + '_lvchgcoefs'] * datum['lvchg'] + \
                                  datum[in_name + '_trchgcoefs'] * datum['trchg']

    def generate_results(self):
        self._ref_trend()
        self._ref_inflect()
        self._comparison_trend()
        self._comparison_inflect()
        self._dif_trend()
        self._dif_inflect()
        try:
            self._regression('lineReference2', 'ReferenceRegress')
            self._regression('lineComparison2', 'ComparisonRegress')
        except:
            pass
