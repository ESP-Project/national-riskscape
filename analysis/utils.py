import arrow
from django.db.models import Min, Max
from modeled_data.models import NationalLevel, StateLevel, CountyLevel, ZipLevel, CensusLevel

def get_time_range(level):
    data_model = None
    first = None
    last = None
    if level == 'national':
        data_model = NationalLevel
    elif level == 'state':
        data_model = StateLevel
    elif level == 'county':
        data_model = CountyLevel
    elif level == 'zip':
        data_model = ZipLevel
    elif level == 'tract':
        data_model = CenusLevel
    if data_model:
        first = data_model.objects.aggregate(Min('year_month'))['year_month__min']
        last = data_model.objects.aggregate(Max('year_month'))['year_month__max']

    if first and last:
        first_year,first_month=year_month_int(first)
        last_year,last_month=year_month_int(last)
        start, end = arrow.get(first_year, first_month, 1), arrow.get(last_year, last_month, 1)
        drm = arrow.Arrow.range('month', start, end)
        dry = arrow.Arrow.range('year', start, end)
        return drm, dry, start, end
    return None, None, None, None

def year_month_int(year_month_str):
    ym = year_month_str.split('-')
    year = int(ym[0]) if ym else None
    month = int(ym[1]) if ym else None
    return year,month

def year_month_str(year, month, sep):
    month_str = '0' + str(month) if month and (int(month) < 10) else str(month)
    year_month = str(year) + sep + month_str
    return year_month

def conditional_decorator(dec, condition):
    def decorator(func):
        if not condition:
            # Return the function unchanged, not decorated.
            return func
        return dec(func)
    return decorator

