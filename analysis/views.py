import arrow, math, csv, subprocess
from datetime import datetime

from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model
from django.conf import settings
from django.urls import reverse
from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import HttpResponse, get_object_or_404, redirect
from django.db.models import Min, Max
from django.core.exceptions import ObjectDoesNotExist

from . import forms
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.models import County
from analysis import jobs
from analysis.stats import Stats
from analysis.utils import get_time_range, conditional_decorator
from analysis.forms import OutcomeQueryForm, FilterQueryForm
from analysis.models import MapResultCache, CurrentRateResultCache
from modeled_data.models import NationalLevel, StateLevel, CountyLevel, ZipLevel
from modeled_data.models import CensusLevel, UploadData
from modeled_data.models import STATES, CONDITIONS
from django_otp.decorators import otp_required
from riskscape_registration.models import Profile

import logging 

logger = logging.getLogger('national_riskscape')

def get_condition(result_cache):
    outcome_form = OutcomeQueryForm(result_cache.outcome)
    if outcome_form.is_valid():
        code= outcome_form.cleaned_data['condition']
        conditions = dict((x,y) for x,y in CONDITIONS)
        if code in conditions:
            return conditions[code]
    return ''

def get_demographic(result_cache):
    demographic = ''
    demographic_value = ''
    filter_form = FilterQueryForm(result_cache.filter)
    if filter_form.is_valid():
        demographic = filter_form.cleaned_data['demographic']
        if demographic == 'overall':
            demographic_value = 'overall'
        else:
            demographic_value = filter_form.cleaned_data[demographic]
    return demographic, demographic_value

def get_month_year(result_cache):
    return str(result_cache.month) +'-' + str(result_cache.year)

def get_county_name(fips):
        if fips and fips.isnumeric():
            try:
                county = County.objects.get(fips=fips)
                if county:
                    return county.name
            except:
                return None
        return None

def get_location_name(location):
    us = dict((x,y) for x,y in STATES)
    if location in us:
        return us[location]
    county_name = get_county_name(location)
    if county_name:
        return county_name
    return location

@login_required
def user_report_csv(request):
    if not request.user.is_superuser:
        return HttpResponseBadRequest('')
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
    content_type='text/csv',
    headers={'Content-Disposition': 'attachment; filename="user_report.csv"'},
    )
    # write csv header
    writer = csv.writer(response)
    writer.writerow(['full name','institution','email', 'active', 'staff', 'superuser', 'last login date',])
    # write user info
    User = get_user_model()
    users = User.objects.all()
    for u in users:
        p = Profile.objects.get(user=u)
        user_last_login_date = u.last_login.strftime("%Y-%m-%d") if u.last_login else None
        institutions = dict(settings.INSTITUTIONS)
        user_institution = institutions[p.institution] if p.institution in institutions else None
        if u.username and u.username != 'uml':
            writer.writerow([
                        p.full_name,
                        user_institution,
                        u.email,
                        u.is_active,
                        u.is_staff,
                        u.is_superuser,
                        user_last_login_date,
                        ])

    # write download date
    writer.writerow([])
    now = datetime.now().strftime("%Y-%m-%d")
    writer.writerow(['Download Date: ', now])

    return response

@login_required
def get_timeseries_csv(request, pk1, pk2):
    crc1 = None
    crc2 = None
    if pk1:
        crc1 = TimeseriesResultCache.objects.get(pk=pk1)
    if pk2 !='-1':
        crc2 = TimeseriesResultCache.objects.get(pk=pk2)

    # get condition, month-year,demographic(stratifier), and results data
    data1 = []
    data2 = []
    if crc1:
        condition1 = get_condition(crc1)
        demographic1, demographic_value1 = get_demographic(crc1)
        location1 = get_location_name(crc1.location)
        data1 = crc1.result
    if crc2:
        condition2 = get_condition(crc2)
        demographic2, demographic_value2 = get_demographic(crc2)
        location2 = get_location_name(crc2.location)
        data2 = crc2.result

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
    content_type='text/csv',
    headers={'Content-Disposition': 'attachment; filename="timeseries_data.csv"'},
    )
    # write data
    writer = csv.writer(response)
    writer.writerow(['Condition','Month-Year','Location',
                     'Group','Group value','Prevalence',])
    for demo_val,data in data1.items(): 
        for d in data:
            month_year = str(d['month']) + '-' + str(d['year'])
            prev = d['prevalence'] if d['prevalence'] >= 0 else None
            writer.writerow([condition1, month_year, location1, 
                            demographic1, demo_val, prev])
    if data2:
        for demo_val,data in data2.items(): 
            for d in data:
                month_year = str(d['month']) + '-' + str(d['year'])
                prev = d['prevalence'] if d['prevalence'] >= 0 else None
                writer.writerow([condition2, month_year, location2, 
                            demographic2, demo_val, prev])
    # write download date
    writer.writerow([])
    now = datetime.now().strftime("%Y-%m-%d")
    writer.writerow(['Download Date: ', now])

    return response

@login_required
def get_current_csv(request, pk1, pk2):
    crc1 = None
    crc2 = None
    if pk1:
        crc1 = CurrentRateResultCache.objects.get(pk=pk1)
    if pk2 !='-1':
        crc2 = CurrentRateResultCache.objects.get(pk=pk2)

    # get condition, month-year,demographic(stratifier), and results data
    data1 = []
    data2 = []
    if crc1:
        condition1 = get_condition(crc1)
        demographic1, demographic_value1 = get_demographic(crc1)
        month_year1 = get_month_year(crc1)
        location1 = get_location_name(crc1.location)
        data1 = crc1.result
    if crc2:
        condition2 = get_condition(crc2)
        demographic2, demographic_value2 = get_demographic(crc2)
        month_year2 = get_month_year(crc2)
        location2 = get_location_name(crc2.location)
        data2 = crc2.result

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(
    content_type='text/csv',
    headers={'Content-Disposition': 'attachment; filename="demographic_data.csv"'},
    )
    # write data
    writer = csv.writer(response)
    writer.writerow(['Condition','Month-Year','Location','Group','Group value',
                    'Num Patients','Prevalence','SE'])
    for d in data1: 
        writer.writerow([condition1, month_year1, location1, demographic1, 
                        d[demographic1], d['npats'], d['prevalence'], d['se']])
    for d in data2: 
        writer.writerow([condition2, month_year2, location2, demographic2, 
                        d[demographic2], d['npats'], d['prevalence'], d['se']])

    # write create date
    writer.writerow([])
    now = datetime.now().strftime("%Y-%m-%d")
    writer.writerow(['Download Date: ', now])

    return response

@login_required
def get_map_csv(request, pk):
    #mrc = get_object_or_404(MapResultCache, pk=pk)
    mrc = MapResultCache.objects.get(pk=pk)
    if mrc.result is not None:
        # condition
        condition = get_condition(mrc)
        # get demographic and value
        demographic, demographic_value = get_demographic(mrc)
        # month & year
        month_year = get_month_year(mrc)

        # Create the HttpResponse object with the appropriate CSV header.
        response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="map_data.csv"'},
        )
        writer = csv.writer(response)

        # get map data
        data = mrc.result

        # save level 2 map data (state, county, zip)
        level2 = data['map_data']
        writer.writerow(['Condition','Month-Year','Group','Group value','Location','Num Patients','Prevalence','SE','Lower 95% CI','Upper 95% CI'])
        for d in level2:
            place = d['place_name'].split(':')
            place_name = place[1] if len(place) > 1 else d['place_name']
            writer.writerow([condition, month_year, demographic, demographic_value, 
                                str(place_name), d['population'], 
                                d['prevalence'], d['se'],d['lowerci'],d['upperci']])

        # save level 1 map data (US or state)
        level1 = data['nationwide']
        writer.writerow([])
        writer.writerow([level1['place_name']])
        writer.writerow(['Prevalence', level1['prevalence']])
        writer.writerow(['SE', level1['se']])
        writer.writerow(['Lower 95% CI', level1['lowerci']])
        writer.writerow(['Upper 95% CI', level1['upperci']])
        writer.writerow(['Num Patients', level1['npats']])
        writer.writerow([])
        now = datetime.now().strftime("%Y-%m-%d")
        writer.writerow(['Download Date: ', now])

        return response

def timeseries(data, outcome_form, filter_form, geo_form, stratify, chunked, force_cache_update=False):
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    from analysis import jobs
    for datum in data:
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                               filter_form=filter_form,
                                                               geo_form=geo_form,
                                                               location=datum['location'],
                                                               time_chunk=chunked,
                                                               stratify=stratify)
        if force_cache_update or trc is None or trc.result is None:
            if trc is None:
                trc = TimeseriesResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                              filter_form=filter_form,
                                                              geo_form=geo_form,
                                                              result=None,
                                                              location=datum['location'],
                                                              time_chunk=chunked,
                                                              stratify=stratify)
            jobs.generate_timeseries_cache.delay(trc.pk, geo_area)
        datum['cache'] = trc.pk

    return data

def current_rates(data, outcome_form, filter_form, geo_form, 
                  stratify, month, year):
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    if outcome_form.is_valid():
        pass
    if filter_form.is_valid():
        pass
    if geo_form.is_valid():
        pass
    from analysis import jobs
    for datum in data:
        crc = CurrentRateResultCache.objects.get_cached_results(outcome_form=outcome_form, 
                                                                filter_form=filter_form,
                                                                location=datum['location'],
                                                                stratify=stratify,
                                                                geo_form=geo_form,
                                                                month=month,
                                                                year=year
                                                                )
        if crc is None or crc.result is None:
            if crc is None:
                crc = CurrentRateResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                               filter_form=filter_form,
                                                               result=None,
                                                               location=datum['location'], 
                                                               stratify=stratify,
                                                               geo_form=geo_form,
                                                               month=month,
                                                               year=year
                                                               )
            jobs.generate_current_rate_cache.delay(crc.pk, geo_area, month, year)

        datum['cache'] = crc.pk

    return data

def get_quarter(month):
    return "Q{}".format(math.ceil(month / 3))

def get_data_time_range():
    first = NationalLevel.objects.aggregate(Min('year_month'))['year_month__min']
    last = NationalLevel.objects.aggregate(Max('year_month'))['year_month__max']
    if first and last:
        first_ym = first.split('-')
        first_year = int(first_ym[0])
        first_month = int(first_ym[1])
        last_ym = last.split('-')
        last_year = int(last_ym[0])
        last_month = int(last_ym[1])
        start, end = arrow.get(first_year, first_month, 1), arrow.get(last_year, last_month, 1)
        drm = arrow.Arrow.range('month', start, end)
        drq = arrow.Arrow.range('quarter', start, end)
        dry = arrow.Arrow.range('year', start, end)
        return drm, drq, dry, start, end
    return None, None, None, None, None

def unpack_params(request, param, default):
    if request.get_full_path().split('?')[0] not in request.META['HTTP_REFERER']:
        return default
    if request.method == "POST":
        query = request.POST.get('query', "")
    else:
        query = request.META['QUERY_STRING']
    if query == "":
        return default
    for qs in query.split('&'):
        k, v = qs.split("=")
        if k == param:
            if v == '':
                return default
            return v
    return default

# Create your views here.
@login_required
def home(request):
    if not settings.TWO_FACTOR_ENABLED or request.user.is_verified():
        outcome_form, filter_form, geo_form = forms.process_queryform(request)
        last_upload = UploadData.objects.filter(status=2).last()
        last_upload_date = last_upload.processed if last_upload else 'Data not uploaded'

        return TemplateResponse(request, 'analysis/home.html',
            {"landing": True, 
            "outcome_form": outcome_form, 
            "filter_form": filter_form, 
            "geo_form":geo_form,
            "last_upload_date":last_upload_date,
            "staging": settings.STAGING})
    elif settings.TWO_FACTOR_ENABLED:
        return redirect('two_factor:setup')


@login_required
def about(request):
    context = {"about": True}
    return TemplateResponse(request, 'analysis/about.html', context)

@csrf_exempt
@login_required
def upload_ivest(request):
    if request.method == "POST" and settings.UPLOAD_SCRIPT:
        subprocess.call(settings.UPLOAD_SCRIPT)
        upload_msg = "Successfully uploaded data"
    else:
        upload_msg = "Data upload failed. Check upload script: {}".format(settings.UPLOAD_SCRIPT)

    context = {"landing": True, 
               "staging": settings.STAGING,
               "upload_message": upload_msg}
    return TemplateResponse(request, 'analysis/home.html', context)

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def heat_map(request, outcome=None):
    chunked = unpack_params(request, 'chunked', 'month')
    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else default_end_year
    month = ebm if update_time else default_end_month
    month_range, year_range, start, end = get_time_range('state')
    if not end:
        context = {"landing": True, 
               "staging": settings.STAGING,
               "upload_message": 'No map data available'}
        return TemplateResponse(request, 'analysis/home.html', context)
    elif year is None or month is None:
        year = end.year
        month = end.month 
    dynamic_start = None
    dynamic_end = None
    if sby is not None:
        dynamic_start = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        dynamic_end = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}
    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    county_zip_tract = filter_form.cleaned_data['county_zip_tract'] if filter_form.is_valid() else 'county'

    if outcome_form.is_valid():
        pass
    if filter_form.is_valid():
        pass
    if geo_form.is_valid():
        pass
    mrc = MapResultCache.objects.get_cached_results(
                                                    geo_form=geo_form,
                                                    outcome_form=outcome_form, 
                                                    filter_form=filter_form,
                                                    month=month,
                                                    year=year)
    if mrc is None or mrc.result is None:
        mrc = MapResultCache.objects.cache_results(outcome_form=outcome_form, 
                                                    filter_form=filter_form, 
                                                    geo_form= geo_form,
                                                    result=None,
                                                    month=month,
                                                    year=year)
        jobs.generate_map_cache.delay(mrc.pk, geo_area, month, year)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form,
                             "geo_form":geo_form,
                             "geo_area":geo_area,
                             "county_zip_tract":county_zip_tract,
                             "chunked": chunked,
                             "dynamic_start":dynamic_start,
                             "dynamic_end":dynamic_end})
@login_required
def map_overlay(request, pk):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        if request.method == 'GET':
            mrc = get_object_or_404(MapResultCache, pk=pk)
            if mrc.result is not None:
                return JsonResponse({"complete": True, "data": mrc.result})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)

def _graph_view(request, pk1, pk2, model, inflection=None):
    if request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':
        if request.method == 'GET':
            pks = [pk1]
            if pk2 != '-1':
                pks.append(pk2)
            data = {}
            data2 = {}
            data3 = {}
            for pk in pks:
                rc = get_object_or_404(model, pk=pk)
                if rc.result is not None:
                    if model == TimeseriesResultCache:
                        if inflection is not None:
                            inflection = int(inflection)
                        rc.result = _set_timeseries_bounds(request, rc.result)
                        data[rc.location] = timeseries_trendline(rc.result, inflection=inflection)
                    else:
                        data[rc.location] = rc.result
                        data2[rc.location] = rc.result2
                        data3[rc.location] = rc.result3
            if len(data) == len(pks):
                return JsonResponse({"complete": True, "data": data, "data2": data2, "data3": data3})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)

def _set_timeseries_bounds(request, result):
    sm = request.GET.get('sbm')
    sy = request.GET.get('sby')
    em = request.GET.get('ebm')
    ey = request.GET.get('eby')

    try:
        start = arrow.get(int(sy), int(sm), 1)
        end = arrow.get(int(ey), int(em), 1)
    except TypeError:
        return result

    bound_result = {}
    for stratification in result:
        strat = []
        for point in result[stratification]:
            point_time = arrow.get(point['year'], point['month'], 1)
            if start <= point_time <= end:
                strat.append(point)
        bound_result[stratification] = strat

    return bound_result

def timeseries_trendline(graphable, inflection=None):
    for stratification in graphable:
        stats_input = []
        enum = graphable[stratification]
        for idx, datum in enumerate(enum):
            value = datum['prevalence'] #(float(datum['numerator']) / float(datum['denominator'])) * 100.0
            if datum['prevalence'] == -1:
                value = None
            stats_input.append(
                {'time': idx,
                 'Reference': value})
        stats = Stats(stats_input, inflection_point=0 if inflection is None else inflection)
        stats.generate_results()

        raw, graphable[stratification], = graphable[stratification], {}
        graphable[stratification]['trend'] = {}
        graphable[stratification]['raw'] = raw
        if inflection is None:
            graphable[stratification]['trend']['points'] = [datum.get('lineReference', 0) for datum in stats.data]
            try:
                graphable[stratification]['trend']['info'] = stats.ref_trend.summary().as_text()
            except AttributeError:
                graphable[stratification]['trend']['info'] = None
        else:
            before = [datum.get('lineReference', 0) for datum in stats.data]
            after = [datum.get('lineReference2', 0) for datum in stats.data]
            graphable[stratification]['trend']['points'] = before[:inflection + 1] + ([None] *
                                                                                      (len(before) - inflection + 1))
            graphable[stratification]['inflect'] = {}
            graphable[stratification]['inflect']['points'] = ([None] * inflection) + after[inflection:]
            try:
                graphable[stratification]['trend']['info'] = stats.ref_trend.summary().as_text()
            except AttributeError:
                graphable[stratification]['trend']['info'] = None
            try:
                graphable[stratification]['inflect']['info'] = stats.ref_inflect.summary().as_text()
            except AttributeError:
                graphable[stratification]['inflect']['info'] = None

    return graphable

@login_required
@conditional_decorator(otp_required,settings.TWO_FACTOR_ENABLED)
def graph_view(request, graph_type, outcome=None):
    outcome_form, filter_form, geo_form = forms.process_queryform(request, outcome)
    geo_area = geo_form.cleaned_data['geo_area'] if geo_form.is_valid() else settings.GEO_AREA_DEFAULT
    if outcome_form is None:
        return redirect(reverse('map'))
    if outcome_form.is_valid() and filter_form.is_valid() and geo_form.is_valid():
        pass

    geo_location = settings.GEO_PLACES[geo_area]['GEO_LOCATION']
    geo_code = settings.GEO_PLACES[geo_area]['GEO_CODE']
    default = geo_code   + ',' + geo_code

    data = [{'location': '','location_label':''}, {'location': '','location_label':''}]
    locations = unpack_params(request, 'locations', default).split(',')
    data[0]['location'] = locations[0]
    data[0]['location_label'] = locations[0]
    try:
        data[1]['location'] = locations[1]
        data[1]['location_label'] = locations[1]
    except IndexError:
        data[1]['location'] = locations[0]
        data[1]['location_label'] = locations[0]


    stratify = filter_form.cleaned_data['demographic'] if filter_form.is_valid() else 'overall'
    #stratify = unpack_params(request, 'stratify',
    #                         'age_group' if graph_type == "current" else 'none')

    chunked = unpack_params(request, 'chunked', 'month')


    county_zip_tract = filter_form.cleaned_data['county_zip_tract'] if filter_form.is_valid() else 'county'
    states = None
    counties = None
    zips = None
    tracts = None
    us_states = dict((x,y) for x,y in STATES)
    if geo_code == 'US':
        states = StateLevel.objects.order_by('state').distinct('state').values_list('state', flat=True)
    else:
        if data[0]['location'] in us_states:
            data[0]['location_label'] = us_states[data[0]['location']]
        if data[1]['location'] in us_states:
            data[1]['location_label'] = us_states[data[1]['location']]
        counties = CountyLevel.objects.filter(state=geo_code).order_by('county_fips').distinct('county_fips').values_list('county_fips', flat=True)
        zips = ZipLevel.objects.filter(state=geo_code).order_by('zip').distinct('zip').values_list('zip', flat=True)
        tracts = CensusLevel.objects.order_by('census_tract').distinct('census_tract').values_list('census_tract', flat=True)

    state_list =[]
    if states:
        for s in states:
            state = {"name":s, "label":us_states[s]}
            state_list.append(state)
            if data[0]['location'] == state['name']:
                data[0]['location_label'] = state['label']
            if data[1]['location'] == state['name']:
                data[1]['location_label'] = state['label']
    county_list = []
    if counties:
        for cfips in counties:
            try:
                cobj = County.objects.get(fips=cfips)
                county = {"name":cfips, "label":cobj.name}
                county_list.append(county)
                if data[0]['location'] == cfips:
                    data[0]['location_label'] = county['label']
                if data[1]['location'] == cfips:
                    data[1]['location_label'] = county['label']
            except ObjectDoesNotExist:
                pass
    location_list = {"states": state_list,
                     "counties": county_list,
                     "zips": zips,
                     "tracts": tracts,
                     }

    context = {"stratify": stratify, 
               "outcome_form": outcome_form, 
               "filter_form": filter_form,
               "geo_form": geo_form,
               "geo_area":geo_area,
               "geo_code":geo_code,
               "geo_location":geo_location,
               "static_stratifications": None, 
               "location_list": location_list,
               }

    sby = unpack_params(request, 'sby', None)
    sbm = unpack_params(request, 'sbm', None)
    eby = unpack_params(request, 'eby', None)
    ebm = unpack_params(request, 'ebm', None)
    default_end_year = unpack_params(request, 'ey', None)
    default_end_month = unpack_params(request, 'em', None)
    update_time = (eby != default_end_year) or (ebm != default_end_month)
    year = eby if update_time else default_end_year
    month = ebm if update_time else default_end_month
    month_range, year_range, start, end = get_time_range('state')
    if year is None or month is None:
        year = end.year
        month = end.month 
    if sby is not None:
        context['dynamic_start'] = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': sbm,
                                    'year': sby}
        context['dynamic_end'] = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = current_rates(data, outcome_form, filter_form, geo_form, 
                                            stratify, month, year)
            context['chunked'] = chunked
            template = 'analysis/current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = timeseries(data, outcome_form, filter_form, geo_form, stratify, chunked)
            context['chunked'] = chunked

            template = 'analysis/timeseries.html'
        else:
            return redirect(reverse('map'))
    except AttributeError as e:
        return redirect(reverse('map'))

    tr = TemplateResponse(request, template, context)

    return tr


@login_required
def current_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, CurrentRateResultCache)


@login_required
def timeseries_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache)


@login_required
def timeseries_trend(request, inflection, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache, inflection)
