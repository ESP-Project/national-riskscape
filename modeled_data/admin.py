from django.contrib import admin

# Register your models here.
from modeled_data.models import UploadData
admin.site.register(UploadData)
