from django.apps import AppConfig


class ModeledDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modeled_data'
