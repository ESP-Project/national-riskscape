from django import forms
from modeled_data.models import UploadData


class UploadRawDataForm(forms.ModelForm):
    class Meta:
        model = UploadData
        fields = ['data_file', 'site_name']
