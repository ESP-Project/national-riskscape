import zipfile
from datetime import datetime, date
import os
from shutil import copyfile

from django.db import DataError
from django.db import IntegrityError
from django.db import transaction
from django.db import connection
from django.db.models import Count
from django_rq import job, enqueue
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from modeled_data.models import UploadData, NationalLevel, StateLevel, CountyLevel, ZipLevel 
from national_riskscape.settings import DATA_SITE_COUNT, MEDIA_ROOT
from national_riskscape.settings import NEW_DATA_EMAIL_LIST, DEFAULT_FROM_EMAIL, ADMIN_EMAIL_LIST
from national_riskscape.settings import ORG_SYSTEM, MAIN_SITE_URL
from analysis.models import TimeseriesResultCache, MapResultCache, CurrentRateResultCache

import logging 
logger = logging.getLogger('national_riskscape')

if settings.STAGING:
    historical_rq = 'historical-stg'
else:
    historical_rq = 'historical'

class LoadError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        site -- site that threw the error
        message -- explanation of the error
    """

    def __init__(self, site, message):
        self.site = site
        self.message = message

def _user_email():
    User = get_user_model()
    users = User.objects.filter(is_active=True)
    email_list = []
    for u in users:
        if u.username !='uml' and u.email:
            email_list.append(u.email)
    return email_list

def _send_email(error_message=None):
    # construct email
    if error_message:
        subject = "Data Upload Error on {}".format(ORG_SYSTEM)
        recipient_list = ADMIN_EMAIL_LIST
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM, 
                  'site_url':MAIN_SITE_URL,
                  'error_message': error_message}
        text_msg = render_to_string('load_error_email.txt', context)
        html_msg = render_to_string('load_error_email.html', context)
    else:
        subject = "New Data Upload on {}".format(ORG_SYSTEM)
        if len(NEW_DATA_EMAIL_LIST) > 0:
            recipient_list = NEW_DATA_EMAIL_LIST
        else:
            recipient_list = _user_email()
        from_email = DEFAULT_FROM_EMAIL
        context ={'site_name':ORG_SYSTEM, 'site_url':MAIN_SITE_URL}
        text_msg = render_to_string('new_data_email.txt', context)
        html_msg = render_to_string('new_data_email.html', context)
    # send email
    for to_email in recipient_list:
        send_mail(
            subject,
            text_msg,
            from_email,
            [to_email],
            fail_silently=False,
            html_message=html_msg,
        )

def process_data():
    loadable_data = UploadData.objects.filter(status=0)
    if len(loadable_data.values_list('site_name', flat=True)) == DATA_SITE_COUNT:
        _load_data.delay([ld.pk for ld in loadable_data])


@job(historical_rq)
def _load_data(uploads):
    uploaded = len(uploads)
    for upload in uploads:
        ud = UploadData.objects.get(pk=upload)

        try:
            ud.status = 1
            ud.save()

            _etl(ud)
            ud.data_file.delete(save=True)
            ud.status = 2
            ud.save()
            _send_email()
        except (LoadError, ValueError, DataError, IntegrityError) as e:
            ud.status = 3
            uploaded -= 1
            _send_email(error_message=str(e))
            raise LoadError(ud.site_name, e)
        finally:
            ud.processed = datetime.now()
            ud.save()

def _etl(ud):
    if zipfile.is_zipfile(ud.data_file.path):
        backup_file = os.path.join(MEDIA_ROOT + 'upload', ud.site_name + '_backup.zip')
        copyfile(ud.data_file.path, backup_file)
        path_parts = ud.data_file.path.split('/')
        path = "{}/{}".format('/'.join(path_parts[:-1]), path_parts[-1:][0].split('.')[0])
        with zipfile.ZipFile(ud.data_file.path) as nrs_zip:
            nrs_zip.extractall(path)
            nrs_names = nrs_zip.namelist()
            # truncate result cache tables
            MapResultCache.objects.all().delete()
            CurrentRateResultCache.objects.all().delete()
            TimeseriesResultCache.objects.all().delete()
            # load files
            for fname in nrs_names:
                if fname == 'National_level.csv':
                    NationalLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY modeled_data_nationallevel(condition,year_month,demographic,demographic_value,est_type,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                elif fname == 'State_level.csv':
                    StateLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY modeled_data_statelevel(state,condition,year_month,demographic,demographic_value,est_type,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                elif fname == 'County_level.csv':
                    CountyLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY modeled_data_countylevel(state,county_fips,condition,year_month,est_type,rural_urban,prevalence,se,npats) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                elif fname == 'Zip_level.csv':
                    ZipLevel.objects.all().delete()
                    fname = path + '/' + fname
                    sql = """COPY modeled_data_ziplevel(state,zip,condition,year_month,est_type,rural_urban,prevalence,se,npats) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
