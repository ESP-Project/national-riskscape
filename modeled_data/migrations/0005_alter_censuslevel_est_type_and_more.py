# Generated by Django 4.1.3 on 2023-02-07 21:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modeled_data', '0004_alter_nationallevel_demographic_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='censuslevel',
            name='est_type',
            field=models.CharField(choices=[('modeled', 'modeled'), ('other', 'other tbd')], max_length=7, verbose_name='Estimate Type'),
        ),
        migrations.AlterField(
            model_name='countylevel',
            name='est_type',
            field=models.CharField(choices=[('modeled', 'modeled'), ('other', 'other tbd')], max_length=7, verbose_name='Estimate Type'),
        ),
        migrations.AlterField(
            model_name='nationallevel',
            name='demographic',
            field=models.CharField(choices=[('overall', 'Overall'), ('sex', 'Sex'), ('age_group', 'Age Group'), ('race', 'Race'), ('primary_payer', 'Primary Payer'), ('rural_urban', 'Rural Urban'), ('state_rural_urban', 'Rural Urban')], max_length=50, verbose_name='Demographic'),
        ),
        migrations.AlterField(
            model_name='nationallevel',
            name='est_type',
            field=models.CharField(choices=[('modeled', 'modeled'), ('other', 'other tbd')], max_length=7, verbose_name='Estimate Type'),
        ),
        migrations.AlterField(
            model_name='statelevel',
            name='demographic',
            field=models.CharField(choices=[('overall', 'Overall'), ('sex', 'Sex'), ('age_group', 'Age Group'), ('race', 'Race'), ('primary_payer', 'Primary Payer'), ('rural_urban', 'Rural Urban'), ('state_rural_urban', 'Rural Urban')], max_length=50, verbose_name='Demographic'),
        ),
        migrations.AlterField(
            model_name='statelevel',
            name='est_type',
            field=models.CharField(choices=[('modeled', 'modeled'), ('other', 'other tbd')], max_length=7, verbose_name='Estimate Type'),
        ),
        migrations.AlterField(
            model_name='ziplevel',
            name='est_type',
            field=models.CharField(choices=[('modeled', 'modeled'), ('other', 'other tbd')], max_length=7, verbose_name='Estimate Type'),
        ),
    ]
