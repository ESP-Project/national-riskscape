import os
from datetime import date

from django.db import models, transaction
from django.conf import settings


CONDITIONS = (
        ('HTN','Hypertension'),
        ('HTN-C','Hypertension controlled'),
        ('DIAB','Diabetes'),
        ('SMOKING','Smoking'),
        ('ASTHMA','Asthma'),
        ('OBESITY','Obesity')
        )
DEMOGRAPHICS = (
    ('overall','Overall'),
    ('sex','Sex'),
    ('age_group','Age Group'),
    ('race','Race'),
    ('primary_payer','Primary Payer'),
    ('rural_urban','Rural Urban'),
    )
EST_TYPE = (('modeled','modeled'),('other','other tbd'))
RURAL_URBAN = (('rural','rural'),('urban','urban'))
STATE_RURAL_URBAN = (('mostly_urban','Mostly Urban (<50%)'), 
                     ('mostly_rural','Mostly Rural (50%-99%)'),
                     ('completely_rural','Completely Rural (100%)'))
SEX = (('male','male'),('female','female'))
AGE_GROUP = (('20-24','20-24'),('25-29','25-29'), ('30-34','30-34'), 
            ('35-44','35-44'), ('45-54','45-54'),('55-64','55-64'),
            ('65-74','65-74'), ('75-84','75-84'))
RACE = (('white','white',),('asian','asian'),('black','black'),
        ('hispanic','hispanic'),('other','other'))
PRIMARY_PAYER = (('unknown','Unknown/other/self-pay'),
                 ('commercial', 'BlueCross/Commercial'),
                 ('workerscomp','Workers comp/Auto'),
                 ('medicaid', 'medicaid'),
                 ('medicare','medicare'))
OVERALL = (('overall','overall'))
STATES = (('AL','Alabama'),
('AK','Alaska'),
('AZ','Arizona'),
('AR','Arkansas'),
('CA','California'),
('CO','Colorado'),
('CT','Connecticut'),
('DE','Delaware'),
('FL','Florida'),
('GA','Georgia'),
('HI','Hawaii'),
('ID','Idaho'),
('IL','Illinois'),
('IN','Indiana'),
('IA','Iowa'),
('KS','Kansas'),
('KY','Kentucky'),
('LA','Louisiana'),
('ME','Maine'),
('MD','Maryland'),
('MA','Massachusetts'),
('MI','Michigan'),
('MN','Minnesota'),
('MS','Mississippi'),
('MO','Missouri'),
('MT','Montana'),
('NE','Nebraska'),
('NV','Nevada'),
('NH','New Hampshire'),
('NJ','New Jersey'),
('NM','New Mexico'),
('NY','New York'),
('NC','North Carolina'),
('ND','North Dakota'),
('OH','Ohio'),
('OK','Oklahoma'),
('OR','Oregon'),
('PA','Pennsylvania'),
('RI','Rhode Island'),
('SC','South Carolina'),
('SD','South Dakota'),
('TN','Tennessee'),
('TX','Texas'),
('UT','Utah'),
('VT','Vermont'),
('VA','Virginia'),
('WA','Washington'),
('WV','West Virginia'),
('WI','Wisconsin'),
('WY','Wyoming'))


SITE_CHOICES = settings.SITENAMES

class ModeledDataManager(models.Manager):

    @transaction.atomic
    def load_data(self,site):
        pass
        
def path_and_rename(instance, filename):
    upload_to = 'upload'
    filename = '{}_{}_{}'.format(date.today().strftime('%Y-%m-%d'), instance.site_name, filename)
    return os.path.join(upload_to, filename)

class ModeledData(models.Model):
    condition = models.CharField('Condition',
                                 max_length=50,
                                 choices = CONDITIONS,
                                 null=False,
                                 blank=False)
    year_month = models.CharField('Year Month',
                                  max_length=7,
                                  null=False,
                                  blank=False)
    est_type = models.CharField('Estimate Type',
                              max_length=7,
                              choices = EST_TYPE,
                              null=False,
                              blank=False)
    npats = models.PositiveIntegerField('Number of Patients',
                                  null=True,
                                )
    prevalence = models.FloatField('Prevalence',
                                  null=True,
                                )
    se = models.FloatField('Standard Error',
                                  null=True,
                                )
    class Meta:
        abstract = True

class NationalLevel(ModeledData):

    demographic = models.CharField('Demographic',
                              max_length=50,
                              choices = DEMOGRAPHICS,
                              null=False,
                              blank=False)
    demographic_value = models.CharField('Demographic Value',
                              max_length=200,
                              null=False,
                              blank=False)
    objects = ModeledDataManager()

    class Meta:
        unique_together = ['condition','year_month','demographic','demographic_value','est_type']
    

class StateLevel(ModeledData):
    demographic = models.CharField('Demographic',
                              max_length=50,
                              choices = DEMOGRAPHICS,
                              null=False,
                              blank=False)
    demographic_value = models.CharField('Demographic Value',
                              max_length=200,
                              null=False,
                              blank=False)
    state = models.CharField('State',
                              max_length=2,
                              choices = STATES,
                              null=False,
                              blank=False)

    objects = ModeledDataManager()

    class Meta:
        unique_together = ['state','condition','year_month','demographic','demographic_value','est_type']

class CountyLevel(ModeledData):
    state = models.CharField('State',
                              max_length=2,
                              choices = STATES,
                              null=False,
                              blank=False)
    county_fips = models.CharField('County FIPS',
                              max_length=5,
                              null=False,
                              blank=False)
    rural_urban = models.CharField('Rural or Urban',
                              max_length=32,
                              choices = RURAL_URBAN,
                              null=False,
                              blank=False)
    
    objects = ModeledDataManager()

    class Meta:
        unique_together = ['state','county_fips','condition','year_month','est_type']
        indexes = [models.Index(fields=['state','rural_urban','condition','year_month','est_type'])]

class ZipLevel(ModeledData):
    state = models.CharField('State',
                              max_length=2,
                              choices = STATES,
                              null=False,
                              blank=False)
    zip = models.CharField('Zip code',
                              max_length=5,
                              null=False,
                              blank=False)
    rural_urban = models.CharField('Rural or Urban',
                              max_length=32,
                              choices = RURAL_URBAN,
                              null=False,
                              blank=False)

    objects = ModeledDataManager()

    class Meta:
        unique_together = ['state','zip','condition','year_month','est_type']
        indexes = [models.Index(fields=['state','rural_urban','condition','year_month','est_type'])]


class CensusLevel(ModeledData):
    census_tract = models.CharField('Census tract',
                              max_length=6,
                              null=False,
                              blank=False)
    rural_urban = models.CharField('Rural or Urban',
                              max_length=5,
                              choices = RURAL_URBAN,
                              null=False,
                              blank=False)

    objects = ModeledDataManager()

    class Meta:
        unique_together = ['census_tract','condition','year_month','est_type']
        indexes = [models.Index(fields=['rural_urban','condition','year_month','est_type'])]

def path_and_rename(instance, filename):
    upload_to = 'upload'
    filename = '{}_{}_{}'.format(date.today().strftime('%Y-%m-%d'), instance.site_name, filename)
    return os.path.join(upload_to, filename)


class UploadDataManager(models.Manager):
    def most_recent_status_by_site(self):
        statuses = {}
        for site in model_choices.SITE_CHOICES:
            try:
                statuses[site[1]] = self.filter(site_name=site[0]).latest('id').get_status_display()
            except UploadData.DoesNotExist:
                pass
        return statuses


class UploadData(models.Model):
    data_file = models.FileField(max_length=255, upload_to=path_and_rename)
    site_name = models.CharField(max_length=3, choices=SITE_CHOICES, blank=False, null=False)
    processed = models.DateTimeField(null=True)
    status = models.IntegerField(choices=((0, "Pending"), (1, "Running"), (2, "Success"), (3, "Failure")), default=0)

    objects = UploadDataManager()

    def __str__(self):
        return "{} data {}".format(self.get_site_name_display(), self.get_status_display())


