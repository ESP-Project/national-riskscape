from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from modeled_data.forms import UploadRawDataForm
from modeled_data.models import UploadData

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

@csrf_exempt
def upload_file(request):
    return _process_upload(request, False)


@csrf_exempt
def upload_historic_file(request):
    return _process_upload(request, True)


def _process_upload(request, is_historic):
    if request.method == 'POST':
        form = UploadRawDataForm(request.POST, request.FILES)
        if form.is_valid():
            upload_data = form.save(commit=False)
            upload_data.is_historic = is_historic
            UploadData.objects.filter(site_name=upload_data.site_name, status=0).delete()
            upload_data.save()
            return HttpResponse('Upload Successful')
        else:
            return HttpResponse("Errors %s" % form.errors)
    else:
        return HttpResponse(status=403)

class UploadView(APIView):
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def upload_file(request):
        return _process_upload(request, True)
