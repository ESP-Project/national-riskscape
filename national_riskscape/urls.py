"""national_riskscape URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.contrib.auth.views import ( LoginView, LogoutView, 
        PasswordChangeView, PasswordChangeDoneView, 
        PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView )
from django.conf import settings
from riskscape_registration.views import RiskscapeRegistrationView 
from rest_framework_simplejwt.views import TokenObtainPairView
from analysis import views as analysis_views
from modeled_data import views as md_views
from two_factor.urls import urlpatterns as tf_urls
from two_factor.views import SetupCompleteView

urlpatterns = [
    path('modeled_data/', include('modeled_data.urls')),
    path('admin/', admin.site.urls),
    #path('accounts/login/', LoginView.as_view(template_name='national_riskscape/login.html'), name='login'),
    path('accounts/logout/', LogoutView.as_view(),  name='logout'),
    path('accounts/password_change/', 
        PasswordChangeView.as_view(template_name='national_riskscape/password_change_form.html'), 
        name='password_change'),
    path('accounts/password_change_done/', 
        PasswordChangeDoneView.as_view(template_name='national_riskscape/password_change_done.html'), 
        name='password_change_done'),
    path('accounts/password_reset/', 
        PasswordResetView.as_view(template_name='national_riskscape/password_reset_form.html'), 
        name='password_reset'),
    path('accounts/password_reset_done/', 
        PasswordResetDoneView.as_view(template_name='national_riskscape/password_reset_done.html'), 
        name='password_reset_done'),
    re_path(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(template_name='national_riskscape/password_reset_confirm.html'), 
        name='password_reset_confirm'),
    path('accounts/reset/done/', PasswordResetCompleteView.as_view(template_name='national_riskscape/password_reset_complete.html'), 
        name='password_reset_complete'),
    path('accounts/', include('registration.backends.admin_approval.urls')),
    path('registration/register/',
        RiskscapeRegistrationView.as_view(template_name='riskscape_registration/registration_form.html'),
        name='registration_register'),
    path('', analysis_views.home, name='home'),
    path('map', analysis_views.heat_map, name='map'),
    re_path(r'^map/(?P<pk>\d+)/$', analysis_views.map_overlay, name='map_overlay'),
    re_path(r'^map_data/(?P<pk>\d+)/$', analysis_views.get_map_csv, name='map_data'),
    path('current', analysis_views.graph_view, {"graph_type": "current"}, name='current'),
    re_path(r'^current/(?P<outcome>[\w ]+)$', analysis_views.graph_view, {"graph_type": "current"},
        name='current-outcome'),
    re_path(r'^current/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.current_graph, name='current_cache'),
    re_path(r'^current_data/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.get_current_csv, name='current_data'),
    path('timeseries', analysis_views.graph_view, {"graph_type" : "timeseries"}, name='timeseries'),
    re_path(r'^timeseries/(?P<outcome>[\w ]+)$', analysis_views.graph_view, {"graph_type": "timeseries"},
        name='timeseries-outcome'),
    re_path(r'^timeseries/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.timeseries_graph, name='timeseries_cache'),
    re_path(r'^trendline/(?P<inflection>\d+)/timeseries/(?P<pk1>\d+)/(?P<pk2>-?-?\d+)/$',
        analysis_views.timeseries_trend, name='timeseries_trendline'),
    re_path(r'^timeseries_data/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', analysis_views.get_timeseries_csv, name='timeseries_data'),
    path('about/', analysis_views.about, name='about'),
    path('data', md_views.upload_file),
    path('data/historic/', md_views.upload_historic_file),
    path('django-rq/', include('django_rq.urls')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/upload/', md_views.UploadView.upload_file, name='upload_file'),
    path('api-auth/', include('rest_framework.urls')),
    path('user-report/', analysis_views.user_report_csv, name='user_report'),
    path('upload-ivest-sh/', analysis_views.upload_ivest, name='upload_ivest')
]

if settings.TWO_FACTOR_ENABLED:
    login_path = path('accounts/login/', LoginView.as_view(template_name='two_factor/login.html'), name='login')
    urlpatterns.insert(2,login_path)
    urlpatterns.append(
        path(
        'account/two_factor/setup/complete/',
            SetupCompleteView.as_view(template_name='two_factor/setup_complete.html'),
            name='setup_complete',
        )
    )
    urlpatterns.append(
        path('', include(tf_urls))
    )
else:
    login_path = path('accounts/login/', LoginView.as_view(template_name='national_riskscape/login.html'), name='login')
    urlpatterns.insert(2,login_path)
