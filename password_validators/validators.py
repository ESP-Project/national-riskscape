import re
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from .models import StoredPassword

SALT = 'MWNQvdVxDfj6eAtoqL6TTY7XAONm9HzL5uhoSFD4zn33iKRg3R'

class RepeatedValidator:

    def validate(self, password, user=None):
        # In case there is no user this validator is not applicable, so we return success
        if user is None:
            return None
        else:
            try:
                u = User.objects.get(username=user.username)
            except ObjectDoesNotExist:
                return None

        # get last saved passwords
        hashed_password = make_password(password, salt=SALT)
        last_passwords = StoredPassword.objects.filter(user=user).order_by('-id')[:settings.NUM_LAST_PASSWORDS]
        # check if current hashed password is in last saved passwords
        for p in last_passwords:
            if hashed_password == p.password:
                raise ValidationError(
                    _("The password cannot be the same as the last {} used passwords.".format(settings.NUM_LAST_PASSWORDS)),
                    code='password_no_symbol',
                )

    def password_changed(self, password, user=None):
        # In case there is no user this is not applicable
        if user is None:
            return None

        hashed_password = make_password(password, salt=SALT)
        self._save_password(hashed_password, user)

    def get_help_text(self):
        return _(
            "Your password cannot be the same as the last {} used passwords.".format(settings.NUM_LAST_PASSWORDS)
        )

    def _save_password(self, password, user):
        if password and user:
            saved_password = StoredPassword()
            saved_password.user = user
            saved_password.password = password
            saved_password.save()

class NumberValidator(object):
    def validate(self, password, user=None):
        if not re.findall('\d', password):
            raise ValidationError(
                _("The password must contain at least 1 digit, 0-9."),
                code='password_no_number',
            )

    def get_help_text(self):
        return _(
            "Your password must contain at least 1 digit, 0-9."
        )


class UppercaseValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[A-Z]', password):
            raise ValidationError(
                _("The password must contain at least 1 uppercase letter, A-Z."),
                code='password_no_upper',
            )

    def get_help_text(self):
        return _(
            "Your password must contain at least 1 uppercase letter, A-Z."
        )


class SymbolValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise ValidationError(
                _("The password must contain at least 1 special character: " +
                  "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"),
                code='password_no_symbol',
            )

    def get_help_text(self):
        return _(
            "Your password must contain at least 1 special character: " +
            "()[]{}|\`~!@#$%^&*_-+=;:'\",<>./?"
        )

