from django.contrib import admin

# Register your models here.
from patient_data.models import UploadData
admin.site.register(UploadData)
