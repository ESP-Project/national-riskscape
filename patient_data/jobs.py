import zipfile
from datetime import datetime, date
import os
from shutil import copyfile

from django.db import DataError
from django.db import IntegrityError
from django.db import transaction
from django.db import connection
from django.db.models import Count
from django_rq import job, enqueue
from django.conf import settings

from patient_data.models import UploadData, NationalLevel, StateLevel, CountyLevel, ZipLevel 
from national_riskscape.settings import DATA_SITE_COUNT, MEDIA_ROOT

import logging 
logger = logging.getLogger('national_riskscape')

class LoadError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        site -- site that threw the error
        message -- explanation of the error
    """

    def __init__(self, site, message):
        self.site = site
        self.message = message


def process_data():
    logger.debug("process data")
    loadable_data = UploadData.objects.filter(status=0)
    if len(loadable_data.values_list('site_name', flat=True)) == DATA_SITE_COUNT:
        _load_data.delay([ld.pk for ld in loadable_data])


@job('historical')
def _load_data(uploads):
    uploaded = len(uploads)
    for upload in uploads:
        ud = UploadData.objects.get(pk=upload)

        try:
            ud.status = 1
            ud.save()

            _etl(ud)
            ud.data_file.delete(save=True)
            ud.status = 2
            ud.save()
        except (LoadError, ValueError, DataError, IntegrityError) as e:
            ud.status = 3
            uploaded -= 1
            raise LoadError(ud.site_name, e)
        finally:
            ud.processed = datetime.now()
            ud.save()

def _etl(ud):
    if zipfile.is_zipfile(ud.data_file.path):
        logger.debug("data file path:" + ud.data_file.path)
        backup_file = os.path.join(MEDIA_ROOT + 'upload', ud.site_name + '_backup.zip')
        logger.debug("backup_file: " + backup_file)
        copyfile(ud.data_file.path, backup_file)
        path_parts = ud.data_file.path.split('/')
        path = "{}/{}".format('/'.join(path_parts[:-1]), path_parts[-1:][0].split('.')[0])
        logger.debug("path: " + path)
        with zipfile.ZipFile(ud.data_file.path) as nrs_zip:
            nrs_zip.extractall(path)
            nrs_names = nrs_zip.namelist()
            for fname in nrs_names:
                logger.debug(fname)
                if fname == 'national_level.csv':
                    fname = path + '/' + fname
                    sql = """COPY patient_data_nationallevel(condition,year_month,demographic,demographic_value,est_type,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        logger.debug(sql)
                        cursor.execute(sql)
                elif fname == 'state_level.csv':
                    fname = path + '/' + fname
                    sql = """COPY patient_data_statelevel(state,condition,year_month,demographic,demographic_value,est_type,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        logger.debug(sql)
                        cursor.execute(sql)
                elif fname == 'county_level.csv':
                    fname = path + '/' + fname
                    sql = """COPY patient_data_countylevel(state,county_fips,condition,year_month,est_type,rural_urban,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        logger.debug(sql)
                        cursor.execute(sql)
                elif fname == 'zip_level.csv':
                    fname = path + '/' + fname
                    sql = """COPY patient_data_ziplevel(state,zip,condition,year_month,est_type,rural_urban,npats,prevalence,se) FROM '{}' DELIMITER ',' CSV HEADER""".format(fname)
                    with connection.cursor() as cursor:
                        logger.debug(sql)
                        cursor.execute(sql)

