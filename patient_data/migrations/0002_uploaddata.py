# Generated by Django 4.1.3 on 2022-11-08 18:26

from django.db import migrations, models
import patient_data.models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UploadData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_file', models.FileField(max_length=255, upload_to=patient_data.models.path_and_rename)),
                ('national_file', models.FileField(max_length=255, upload_to=patient_data.models.path_and_rename)),
                ('state_file', models.FileField(max_length=255, upload_to=patient_data.models.path_and_rename)),
                ('county_file', models.FileField(max_length=255, upload_to=patient_data.models.path_and_rename)),
                ('zip_file', models.FileField(max_length=255, upload_to=patient_data.models.path_and_rename)),
                ('site_name', models.CharField(choices=[('UML', 'UMASS Lowel')], max_length=3)),
                ('processed', models.DateTimeField(null=True)),
                ('status', models.IntegerField(choices=[(0, 'Pending'), (1, 'Running'), (2, 'Success'), (3, 'Failure')], default=0)),
            ],
        ),
    ]
