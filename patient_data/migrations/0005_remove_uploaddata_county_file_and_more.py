# Generated by Django 4.1.3 on 2022-11-10 21:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0004_alter_uploaddata_county_file_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploaddata',
            name='county_file',
        ),
        migrations.RemoveField(
            model_name='uploaddata',
            name='national_file',
        ),
        migrations.RemoveField(
            model_name='uploaddata',
            name='state_file',
        ),
        migrations.RemoveField(
            model_name='uploaddata',
            name='zip_file',
        ),
    ]
