# Generated by Django 4.1.3 on 2022-11-11 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0006_alter_countylevel_year_month_and_more'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='nationallevel',
            unique_together=set(),
        ),
        migrations.AlterUniqueTogether(
            name='statelevel',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='countylevel',
            name='group',
        ),
        migrations.RemoveField(
            model_name='countylevel',
            name='group_value',
        ),
        migrations.RemoveField(
            model_name='ziplevel',
            name='group',
        ),
        migrations.RemoveField(
            model_name='ziplevel',
            name='group_value',
        ),
        migrations.AddField(
            model_name='countylevel',
            name='demographic',
            field=models.CharField(choices=[('sex', 'sex'), ('age_group', 'age group'), ('race', 'race'), ('primary_payer', 'primary payer'), ('rural_urban', 'rural urban'), ('overall', 'overall')], default='abc', max_length=50, verbose_name='Demographic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='countylevel',
            name='demographic_value',
            field=models.CharField(default='age', max_length=200, verbose_name='Demographic Value'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='nationallevel',
            name='demographic',
            field=models.CharField(choices=[('sex', 'sex'), ('age_group', 'age group'), ('race', 'race'), ('primary_payer', 'primary payer'), ('rural_urban', 'rural urban'), ('overall', 'overall')], default='age', max_length=50, verbose_name='Demographic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='nationallevel',
            name='demographic_value',
            field=models.CharField(default='age', max_length=200, verbose_name='Demographic Value'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='statelevel',
            name='demographic',
            field=models.CharField(choices=[('sex', 'sex'), ('age_group', 'age group'), ('race', 'race'), ('primary_payer', 'primary payer'), ('rural_urban', 'rural urban'), ('overall', 'overall')], default='age', max_length=50, verbose_name='Demographic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='statelevel',
            name='demographic_value',
            field=models.CharField(default='age', max_length=200, verbose_name='Demographic Value'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ziplevel',
            name='demographic',
            field=models.CharField(choices=[('sex', 'sex'), ('age_group', 'age group'), ('race', 'race'), ('primary_payer', 'primary payer'), ('rural_urban', 'rural urban'), ('overall', 'overall')], default='age', max_length=50, verbose_name='Demographic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ziplevel',
            name='demographic_value',
            field=models.CharField(default='age', max_length=200, verbose_name='Demographic Value'),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='nationallevel',
            unique_together={('condition', 'year_month', 'demographic', 'demographic_value', 'est_type')},
        ),
        migrations.AlterUniqueTogether(
            name='statelevel',
            unique_together={('state', 'condition', 'year_month', 'demographic', 'demographic_value', 'est_type')},
        ),
        migrations.RemoveField(
            model_name='nationallevel',
            name='group',
        ),
        migrations.RemoveField(
            model_name='nationallevel',
            name='group_value',
        ),
        migrations.RemoveField(
            model_name='statelevel',
            name='group',
        ),
        migrations.RemoveField(
            model_name='statelevel',
            name='group_value',
        ),
    ]
