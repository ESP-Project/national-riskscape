from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from riskscape_registration.models import Profile


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'employee'


class UserAdmin(BaseUserAdmin):
    list_display = ('full_name', 'email', 'institution', 
                    'is_active', 'is_staff', 'is_superuser')

    def full_name(self, obj):
        return obj.profile.full_name

    def institution(self, obj):
        return obj.profile.get_institution_display()

    inlines = (ProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
