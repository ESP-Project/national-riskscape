from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField("Full Name", max_length=250)
    institution = models.CharField("Institutional Affiliation", max_length=250, 
                                    choices=settings.INSTITUTIONS)
