from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.urls import reverse
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.template.loader import render_to_string
from django.utils.module_loading import import_string

from registration.models import RegistrationProfile
from registration.views import ActivationView
from registration.backends.admin_approval.views import RegistrationView

from riskscape_registration.models import Profile
from national_riskscape import settings

REGISTRATION_FORM_PATH = getattr(settings, 'REGISTRATION_FORM',
                                 'registration.forms.RegistrationForm')
REGISTRATION_FORM = import_string(REGISTRATION_FORM_PATH)

REGISTRATION_SUPP_FORM_PATH = getattr(settings, 'REGISTRATION_SUPPLEMENT_FORM',
                                 'riskscape_registration.forms.UserProfileForm')
REGISTRATION_SUPP_FORM = import_string(REGISTRATION_SUPP_FORM_PATH)

class RiskscapeRegistrationView(RegistrationView):
    def form_valid(self, form, supplement_form=None):
        new_user = self.register(form)

        if supplement_form:
            supplement = supplement_form.save(commit=False)
            supplement.user = new_user
            supplement.save()

        success_url = self.get_success_url(new_user)

        # success_url may be a simple string, or a tuple providing the
        # full argument set for redirect(). Attempting to unpack it
        # tells us which one it is.
        try:
            to, args, kwargs = success_url
        except ValueError:
            return redirect(success_url)
        else:
            return redirect(to, *args, **kwargs)

    def get_supplement_form(self, supplement_form_class):
        """get registration supplement form instance"""
        if not supplement_form_class:
            return None
        return supplement_form_class(**self.get_form_kwargs())

    def form_invalid(self, form, supplement_form=None):
        context = self.get_context_data(
            form=form,
            supplement_form=supplement_form
        )
        return self.render_to_response(context)

    def get(self, request, *args, **kwargs):
        form_class = REGISTRATION_FORM
        form = self.get_form(form_class)
        supplement_form_class = REGISTRATION_SUPP_FORM
        supplement_form = self.get_supplement_form(supplement_form_class)
        context = self.get_context_data(
                form=form, supplement_form=supplement_form)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form_class = REGISTRATION_FORM
        form = self.get_form(form_class)
        supplement_form_class = REGISTRATION_SUPP_FORM
        supplement_form = self.get_supplement_form(supplement_form_class)
        if form.is_valid() and (not supplement_form or supplement_form.is_valid()):
            return self.form_valid(form, supplement_form)
        else:
            return self.form_invalid(form, supplement_form)

